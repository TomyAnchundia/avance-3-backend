CREATE TABLE laboratorio (
    id_laboratorio SERIAL ,
    nombre_laboratorio VARCHAR(30) UNIQUE NOT NULL,
    numero_maquinas INTEGER  NOT NULL,
    estado_laborartorio BOOLEAN NOT NULL,
    CONSTRAINT PK_LABORATORIO PRIMARY KEY (id_laboratorio)
);





-- CREATE TABLE usuario (
--     id_usuario SERIAL ,
--     nombre_usuario VARCHAR(30) UNIQUE NOT NULL ,
--     contrasena_usuario VARCHAR(30) NOT NULL,
--     usuario_admin BOOLEAN NOT NULL,
--     CONSTRAINT PK_USUARIO PRIMARY KEY (id_usuario)
-- );


CREATE TABLE materia (
    id_materia SERIal ,
    nombre_materia VARCHAR(255) UNIQUE NOT NULL,
    CONSTRAINT PK_MATERIA PRIMARY KEY (id_materia)
);
CREATE TABLE observacion (
    id_observacion SERIAL  ,
    descripcion_observacion VARCHAR(255) NOT NULL,

    CONSTRAINT PK_OBSERVACION PRIMARY KEY (id_observacion)

);

CREATE TABLE practica(
    id_practica SERIAL  ,
    descripcion_practica VARCHAR(255) NOT NULL,

    CONSTRAINT PK_PRACTICA PRIMARY KEY (id_practica)
);

CREATE TABLE docente (
    id_docente SERIAL  ,
    -- id_usuario INTEGER  NULL,
    id_materia INTEGER NULL,
    nombre_docente VARCHAR(40) NOT NULL,
    apellido_docente VARCHAR(40) NOT NULL,
    telefono_docente INTEGER NOT NULL,
    correo_docente VARCHAR(50) NOT NULL,
    nombre_usuario VARCHAR(11) UNIQUE NOT NULL ,
    contrasena_usuario VARCHAR(30) NOT NULL,
    usuario_admin BOOLEAN NOT NULL,
    CONSTRAINT PK_DOCENTE PRIMARY KEY (id_docente)
);

CREATE TABLE detalle_reserva (
    id_detalle_reserva  SERIAL  ,
    id_practica INTEGER  NULL,
    numero_maquinas_usar INTEGER NOT NULL,
    fecha_reserva DATE NOT NULL,
    hora_inicio TIME NOT NULL,
    hora_fin TIME NOT NULL,

    CONSTRAINT PK_DETALLE_RESERVA PRIMARY KEY (id_detalle_reserva)
);




CREATE TABLE reservar_laboratorio (
    id_reservar_laboratorio SERIAL,
    id_detalle_reserva INTEGER  NULL,
    id_laboratorio INTEGER  NULL,
    id_docente INTEGER  NULL,
    CONSTRAINT PK_RESERVAR_LABORATORIO PRIMARY KEY (id_reservar_laboratorio)
);

CREATE TABLE entregar_laboratorio (
    id_entregar_laboratorio SERIAL  ,
    id_observacion INTEGER NOT NULL,
    id_reservar_laboratorio INTEGER NOT NULL,
    fecha_entrega TIMESTAMP NOT NULL,

    CONSTRAINT PK_ENTREGAR_LABORATORIO PRIMARY KEY (id_entregar_laboratorio)
);



-- ALTER TABLE docente 
--     add CONSTRAINT FK_DOCENTE_TIENE_USUARIO FOREIGN KEY (id_usuario)
--         REFERENCES usuario (id_usuario)
--         ON DELETE restrict ON UPDATE restrict;

ALTER TABLE docente
    add CONSTRAINT FK_DOCENTE_IMPARTE_MATERIAS FOREIGN KEY (id_materia)
        REFERENCES materia (id_materia)
        ON DELETE restrict ON UPDATE restrict;


ALTER TABLE detalle_reserva 
    add CONSTRAINT FK_DETALLE_RESERVA_TIENE_PRACTICA FOREIGN KEY (id_practica)
        REFERENCES practica (id_practica)
        ON DELETE restrict ON UPDATE restrict;



ALTER TABLE reservar_laboratorio 
    add CONSTRAINT FK_RESERVAR_LABORATORIO_TIENE_DETALLES_RESERVA FOREIGN KEY (id_detalle_reserva)
        REFERENCES detalle_reserva (id_detalle_reserva)
        ON DELETE restrict ON UPDATE restrict;

ALTER TABLE reservar_laboratorio
    add CONSTRAINT FK_RESERVAR_LABORATORIO_TIENE_LABORATORIO FOREIGN KEY (id_laboratorio)
        REFERENCES laboratorio (id_laboratorio)
        ON DELETE restrict ON UPDATE restrict;

ALTER TABLE reservar_laboratorio
    add CONSTRAINT FK_RESERVAR_LABORATORIO_TIENE_DOCENTE FOREIGN KEY (id_docente)
        REFERENCES docente (id_docente)
        ON DELETE restrict ON UPDATE restrict;